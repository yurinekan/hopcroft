import './styles.css'

function CardRelation({ name, information, id, setRelation, type, breed }) {
// console.log({entityArray})
  // {}
  const handleOnClick = () => {
    setRelation(prevRelation => {
      if (prevRelation.length === 0) {
        return [
          ...prevRelation, 
          { [type]: id }
        ]
      } else {
        const size = prevRelation.length;

        const lastRelation = prevRelation[size - 1];
        
        const hasRelation =  Object.keys(lastRelation).length === 2;

        if (!hasRelation) {
          const parseRelation = [...prevRelation]
          parseRelation.pop()
          // console.log('bbbb', id)
          // console.log(...parseRelation)
          return [
            ...parseRelation, 
            { ...lastRelation, [type]: id },
            
          ]
        }
        return [
          ...prevRelation, 
          { [type]: id }
        ]
      }
    })
  }

  return (
    <div className={`information-container ${id}`} onClick={handleOnClick}>
      <h4>Nome: { name }</h4>
      <h4>{breed}: { information }</h4>
    </div>
  )
}

export default CardRelation;