import { useEffect, useState } from 'react';
import LineTo from 'react-lineto';
import CardRelation from '../../components/CardRelation';
import api from '../../services/api';
import { Container, ListContainer, List, Header } from './styles';


function initialState() {
  return { name: '', type: '', tel: '', breed: '' };
}

function InitialPage() {
  const [users, setUsers] = useState([])
  const [cats, setCats] = useState([])
  const [values, setValues] = useState(initialState)
  const [relations, setRelations] = useState([])
  const [matches, setMatches] = useState([]);

  const handleChange = (e) => {
    const { value, name } = e.target
    setValues({
      ...values,
      [name]: value,
    })
    console.log(values)
  }

  const handleSubmit = async (e) => {
    let data;
    console.log(values.type)
    if (values.type === 'user') {
      data = {
        name: `${values.name}`,
        phone: `${values.tel}`
      }
    }
    if (values.type === 'cat') {
      data = {
        name: `${values.name}`,
        breed: `${values.breed}`
      }
    }
    createEntity(`${values.type}s`, data)
    console.log(data)
  }

  const createEntity = async (route, data) => {
    api.post(`${route}`, data)
      .then(res => console.log(res))
      .catch(err => console.error(err))
  }

  const handleMatch = async () => {
    const relationParse = relations.map(relation => {
      const [_u, userPosition] = relation.user.split('-');
      const [_c, catPosition] = relation.cat.split('-');

      return [userPosition, catPosition]
    })
    const { data } = await api.post('emparelhamento', {
      m: users.length,
      n: cats.length,
      edges: relationParse
    })

    setMatches(data.data);
  }

  const isHighlighter = (userId, catId) => {
    let isLighter = false;
    if (!userId || !catId) return false
    
    const [userIdPos, catIdPos] = [userId.split('-')[1], catId.split('-')[1]]

    for (let i = 0; i < matches.length; i++) {
      const match = matches[i];
      const [userPos, catPos] = match;

      if (Number(userPos) === Number(userIdPos) && Number(catPos) === Number(catIdPos)) return true;
    }
    
    return isLighter;
  }

  useEffect(() => {
    api.get('users').then(data => {
      const responseUsers = data.data.data
      setUsers(responseUsers)
    })
    api.get('cats').then(data => {
      const responseCats = data.data.data
      setCats(responseCats)
    })
  }, [])

  const setColor = (a, b) => {
    return isHighlighter(a, b) ? "#ff0000" : "#89ce89"
  }
  
  return (
    <Container>
      <div id="content">
        <Header>
          <div>
            <form onSubmit={handleSubmit}>

              <label htmlFor="type">Tipo</label>
              <select onChange={handleChange} name="type" id="">
                <option hidden value="">Selecionar</option>
                <option value="user">Usuário</option>
                <option value="cat">Gato</option>
              </select>

              <label htmlFor="name">Nome</label>
              <input onChange={handleChange} placeholder="Joaquim" type="text" name="name" />

              <label htmlFor="breed">Raça</label>
              <input disabled={values.type !== 'cat'} onChange={handleChange} placeholder="Persa" type="text" name="breed" />

              <label htmlFor="tel">Número de telefone</label>
              <input disabled={values.type !== 'user'} onChange={handleChange} placeholder="(85)929384912" type="tel" name="tel" />

              <button type="submit">Criar</button>
            </form>
          </div>
        </Header>
        <ListContainer>
          <List>
            <h2>Usuários</h2>
            {users.map((user, index) => (
              <CardRelation 
                key={user._id} 
                name={user.name} 
                information={user.phone} 
                id={`${user._id}-${index}`} 
                setRelation={setRelations}
                type="user"
                breed="Telefone"
              />
            ))}
          </List>

          <List>
            <h2>Gatos</h2>
            {cats.map((cat, index) => (
              <CardRelation 
                key={cat._id} 
                name={cat.name} 
                information={cat.breed} 
                id={`${cat._id}-${index}`} 
                setRelation={setRelations}
                type="cat"
                breed="Raça"
              />
            ))}
          </List>

          {relations.map(relation => (
            <LineTo 
              key={`${relation?.user}-${relation?.cat}`}
              from={relation?.user ?? ''}
              to={relation?.cat ?? ''} 
              borderWidth={4}
              borderColor={setColor(relation?.user, relation?.cat)}
            />
          ))}

        </ListContainer>
      </div>
      <button onClick={handleMatch}>Fazer o match!</button>
    </Container>
  )
}


export default InitialPage;