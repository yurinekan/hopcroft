import styled from "styled-components";



export const Container = styled.div`
    height: 100vh;
    display: flex;
    align-items: center;
    flex-direction: column;
    #content {
        width: 80%;
    }
`

export const Header = styled.div`
    display: flex;
    justify-content: center;
    
    form {
        input, select {
            box-shadow: .9px .9px 5px #9a9a9a;
            margin-bottom: 10px;
            width: 100%;
            height: 30px;
            border: 0;
            border-radius: 4px;
        }
    }
    button {
        align-items: center;
        text-align: center;
        margin-bottom: 5px;
    }
    label {
        margin: 5px;
    }
`

export const ListContainer = styled.div`
    display: flex;
    justify-content: center;
`

export const List = styled.div`
    display: flex;
    flex-direction: column;
    border-radius: 10px;
    background-color: #ffffff;
    box-shadow: .9px .9px 5px #9a9a9a;
    margin: 16px;
    h2 {
        text-align: center;
    }
`